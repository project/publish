<?php
/**
 * @file
 * Publish module.
 *
 */

define('PUBLISH_AUTHTYPE_NONE', 0);
define('PUBLISH_AUTHTYPE_PERM', 1);
define('PUBLISH_AUTHTYPE_SIMPLE', 2);

define('PUBLISH_WHENPUB_CRON', 0);
define('PUBLISH_WHENPUB_NODESUB', 1);

define('PUBLISH_PULL', 0);
define('PUBLISH_PUSH', 1);

/**
 * Implementation of hook_perm().
 */
function publish_perm() {
  return array('administer channels');
}

/**
 * Implementation of hook_menu().
 */
function publish_menu() {
  $items = array();

  $items['admin/content/publish'] = array(
    'title' => 'Publish',
    'description' => 'Manages channels used to publish content from this site to other sites.',
    'page callback' => 'publish_overview',
    'access arguments' => array('administer channels'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/overview'] = array(
    'title' => 'Overview',
    'description' => 'Manages channels used to publish content from this site to other sites.',
    'page callback' => 'publish_overview',
    'access arguments' => array('administer channels'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 1,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/subscribers/fill/%'] = array(
    'title' => 'Fill subscription',
    'description' => 'Adds existing nodes to the queue.',
    'page callback' => 'publish_admin_subscription_fill',
    'page arguments' => array(5),
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/subscribers/delete/%'] = array(
    'title' => 'Delete subscription',
    'description' => 'Deletes a subscription.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('publish_admin_subscription_form_delete', 5),
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/subscribers/%'] = array(
    'title' => 'Subscribers',
    'description' => 'Shows subscribers for a channel.',
    'page callback' => 'publish_subscribers',
    'page arguments' => array(4),
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/add'] = array(
    'title' => 'Add channel',
    'description' => 'Adds a channel.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('publish_admin_channel_form_add'),
    'access arguments' => array('administer channels'),
    'type' => MENU_LOCAL_TASK,
    'weight' => 2,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/edit/%'] = array(
    'title' => 'Edit channel',
    'description' => 'Modifies a channel.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('publish_admin_channel_form_add', 4),
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );

  $items['admin/content/publish/delete/%'] = array(
    'title' => 'Delete channel',
    'description' => 'Deletes a channel.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('publish_admin_channel_form_delete', 4),
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );
  
  $items['admin/content/publish/js'] = array(
    'description' => 'Ahah callback.',
    'page callback' => 'publish_admin_views_ahah',
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
    'file' => 'publish.admin.inc',
  );
  
  $items['sandbox/publish'] = array(
    'title' => 'Sandbox',
    'page callback' => 'publish_sandbox',
    'access arguments' => array('administer channels'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function publish_sandbox() {
  $viewp = explode('-', 'publish_test-page_1');
  $view = views_get_view($viewp[0]);
  if (!$view) {
    continue;
  }
  $view->set_display($viewp[1]);
  $view->set_arguments(array(79));
  $view->execute();
  dpm($view);
  return 'a';
}

function publish_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {
  if ($op == 'insert' || $op == 'update') {
    if ($node->status != 1) {
      return;
    }
    
    $publish_now = FALSE;
    
    $result = db_query('SELECT * FROM {publish_channel} pc INNER JOIN {publish_subscribers} ps ON pc.channel_id = ps.channel_id');
    while ($row = db_fetch_object($result)) {
      // we filter the node using the view
      $viewp = explode('-', $row->view);
      $view = views_get_view($viewp[0]);
      if (!$view) {
        continue;
      }
      $view->set_display($viewp[1]);
      $view->set_arguments(array($node->nid));
      $view->execute();
      if (isset($view->result[0]) && $view->result[0]->nid == $node->nid) {
        if ($row->whenpub == PUBLISH_WHENPUB_NODESUB) {
          $publish_now = TRUE;
        }
        
        publish_queue_delete($node->nid, $row->sid);
        publish_queue_save($node->nid, $row->sid, $row->channel_id, $node->changed);
      }
    }
    
    if ($publish_now) {
      publish_exit(TRUE);
    }      
  }
  
  if ($op == 'delete') {
    publish_queue_delete($node->nid);
  }
}

function publish_exit($set = FALSE) {
  static $do_transfer = FALSE;

  if ($do_transfer) {
    publish_push();
    return;
  }

  if ($set) {
    $do_transfer = TRUE;
  }
}

function publish_push() {
  global $base_url;
  $qtime = variable_get('publish_queue_time', 300); // 5 minutes
  // this loops through the queue (oldest nodes first) with a time window set by qtime
  // qtime should be long enough that an attempt to send each item
  // in the queue can be made within the time window
  // we use db_fetch_object() to get one node at a time
  while ($data = db_fetch_object(db_query("SELECT * FROM {publish_queue} WHERE last_attempt < %d ORDER BY changed", time() - $qtime))) {
    // see if there are any other nodes we can include for this subscriber
    // while we have the overhead of sending xmlrpc anyway
    $node_ids = array();
    $qids = array();
    $result = db_query("SELECT qid, nid, attempts FROM {publish_queue} WHERE sid = %d AND channel_id = %d", $data->sid, $data->channel_id);
    while ($row = db_fetch_object($result)) {
      $node_ids[] = $row->nid;
      $qids[$row->nid] = array('qid' => $row->qid, 'attempts' => $row->attempts);
    }

    $nodes = array();
    foreach ($node_ids as $nid) {
      $node = publish_publish($nid);
      if ($node) {
        $nodes[] = $node;
      }
      else {
        publish_queue_delete($nid);
      }
    }

    $subscription = publish_subscription_load($data->sid);
    $message = array(
      'token'     => $subscription->token,
      'rsid'      => (int)$subscription->sid,
      'count'     => count($nodes),
      'site_url'  => $base_url,
      'site_name' => variable_get('site_name', 'drupal'),
      'nodes'     => $nodes,
      );

    $response = xmlrpc($subscription->url, 'drupal.subscribe.receive', $message);
    if ($response) {
      foreach ($nodes as $node) {
        publish_queue_delete($node->nid, $data->sid);
      }
      db_query('UPDATE {publish_subscribers} SET last_sent = %d WHERE sid = %d', time(), $data->sid);
      watchdog('publish', 'XMLRPC pushed %count nodes to %url (channel id %channel_id)', array('%count' => $message['count'], '%url' => check_plain($subscription->url), '%channel_id' => check_plain($data->channel_id)));
    }
    else {
      $error = xmlrpc_error_msg();
      foreach ($node_ids as $nid) {
        $attempts = $qids[$nid]['attempts'];
        $max_attempts = variable_get('publish_push_max_attempts', 5);
        if ($attempts > $max_attempts) {
          publish_queue_delete($nid, $data->sid);
        }
        else {
          db_query('UPDATE {publish_queue} SET attempts = %d, last_attempt = %d WHERE qid = %d', $attempts + 1, time(), $qids[$nid]['qid']);
        }
      }
      watchdog('publish', 'XMLRPC push failed for %url (channel id %channel_id): %faultString', array('%url' => $subscription->url, '%channel_id' => $data->channel_id, '%faultString' => $error ? $error : t('unavailable')), WATCHDOG_ERROR);
    }
  }
}

function publish_publish($nid) {
  $node = node_load($nid);
  
  if (!$node) {
    return NULL;
  }
  
  // do some cleanup
  unset($node->data);
  unset($node->revisions);
  unset($node->picture);
  unset($node->workspaces);
  $node->url = url('node/' . $nid, array('absolute' => TRUE));
  
  module_invoke_all('publish_prepare', $node);

  return $node;
}

function publish_queue_save($nid, $sid, $channel_id, $changed) {
  $qitem = new stdClass();

  $qitem->nid = $nid;
  $qitem->sid = $sid;
  $qitem->channel_id = $channel_id;
  $qitem->changed = $changed;

  drupal_write_record('publish_queue', $qitem);
}

function publish_queue_delete($nid, $sid = NULL) {
  if ($sid) {
    db_query('DELETE FROM {publish_queue} WHERE nid = %d AND sid = %d', $nid, $sid);
  }
  else {
    db_query('DELETE FROM {publish_queue} WHERE nid = %d', $nid);
  }
}

function publish_channel_load($channel_id) {
  $channel = db_fetch_object(db_query('SELECT * FROM {publish_channel} WHERE channel_id = %d', $channel_id));
  return $channel;
}

function publish_channel_save($values) {
  $channel = new stdClass();
  if (isset($values['channel_id']) && is_numeric($values['channel_id'])) {
    $update = 'channel_id';
  }
  else {
    $update = NULL;
  }
  
  foreach ($values as $row => $value) {
    $channel->{$row} = $value;
  }
  
  drupal_write_record('publish_channel', $channel, $update);
}

function publish_channel_delete($channel_id) {
  if (is_object($channel_id)) {
    $channel_id = $channel_id->channel_id;
  }
  
  db_query('DELETE FROM {publish_channel} WHERE channel_id = %d', $channel_id);
}


function publish_subscription_load($sid) {
  $subscription = db_fetch_object(db_query('SELECT * FROM {publish_subscribers} WHERE sid = %d', $sid));
  return $subscription;
}

function publish_subscription_delete($sid) {
  if (is_object($sid)) {
    $sid = $sid->sid;
  }
  
  // TODO: delete subscription on the remote site as well
  
  db_query('DELETE FROM {publish_queue} WHERE sid = %d', $sid);
  db_query('DELETE FROM {publish_subscribers} WHERE sid = %d', $sid);
}

function publish_subscription_save($channel_id, $username, $pass, $dest, $method, $filters, $sid = NULL) {
  $parts = explode('/', $dest);
  array_pop($parts);
  $base_url = implode($parts, '/');
  $token = md5($username . $pass . $base_url);

  $subscription = new stdClass();

  if ($sid) {
    $sub = db_fetch_object(db_query("SELECT * FROM {publish_subscribers} WHERE sid = %d", $sid));
    $subscription->sid = $sid;
    $update = 'sid';
  }
  else {
    $sub = db_fetch_object(db_query("SELECT * FROM {publish_subscribers} WHERE channel_id = %d AND url = '%s'", $channel_id, $dest));
    if ($sub) {
      $subscription->sid = $sub->sid;
      $update = 'sid';
    }
    else {
      $update = NULL;
    }
  }

  if (!$update) {
    $subscription->token = $token;
    $subscription->created = time();
    $subscription->last_sent = 0;
    $subscription->sub_status = 0;
  }
  else {
    if ($dest != $sub->url) {
      return t('The url of the site that requested this subscription and the url of the site requesting this update do not match.');
    }
  }

  $subscription->channel_id = $channel_id;
  $subscription->url = $dest;
  $subscription->username = $username;
  $subscription->pass = $pass;
  $subscription->method = ($method == 'push') ? PUBLISH_PUSH : PUBLISH_PULL;
  $subscription->filters = serialize($filters);

  drupal_write_record('publish_subscribers', $subscription, $update);

  return $subscription->sid;
}

function publish_xmlrpc() {
  return array(
    array('drupal.publish.maySubscribe', 'publish_xmls_may_subscribe', array('array', 'int', 'string', 'string'), t('Return channel info if caller supplies appropriate credentials.')),
    array('drupal.publish.subscribe', 'publish_xmls_receive_subscription', array('int', 'int', 'string', 'string', 'string', 'string', 'array'), t('Establish relationship with subscriber.')),
    array('drupal.publish.pull', 'publish_xmls_publish', array('array', 'int', 'int', 'string', 'string', 'array'), t('Send nodes matching certain conditions to subscriber.')),
    array('drupal.publish.getNode', 'publish_xmls_get_node', array('array', 'int', 'string', 'string', 'int'), t('Send single node to subscriber.')),
    array('drupal.publish.getChannels', 'publish_xmls_get_channels', array('array'), t('Return list of public channels on this site.')),
    array('drupal.publish.cancelSubscription', 'publish_xmls_cancel_subscription', array('boolean', 'int', 'string'), t('Cancel subscription.'))
  );
}

function publish_xmls_get_node($channel_id, $username, $pass, $nid) {
  $channel = publish_channel_load($channel_id);
  $may_subscribe = publish_may_subscribe($channel, $username, $pass);

  if ($may_subscribe) {
    $node = publish_publish(array($nid));
    return $node;
  }
  else {
    return xmlrpc_error(904, theme('publish_authentication_failure_message', $username));
  }
}

function publish_xmls_cancel_subscription($sid, $token) {
  $data = db_fetch_object(db_query('SELECT token, url FROM {publish_subscribers} WHERE sid = %d', $sid));
  if (!$data) {
    return xmlrpc_error(907, t('Invalid subscription ID.'));
  }
  if ($data->token == $token) {
    db_query('DELETE FROM {publish_subscribers} WHERE sid = %d', $sid);
  }
  else {
    return xmlrpc_error(906, t('Token authentication failed.'));
  }
  return TRUE;  
}

function publish_xmls_receive_subscription($channel_id, $username, $pass, $dest, $method, $filters) {
  $host = gethostbyaddr($_SERVER['REMOTE_ADDR']);

  // should check if $dest is in domain of $host here
  $channel = publish_channel_load($channel_id);
  $response = new stdClass();
  if ($channel) {
    $may_subscribe = publish_may_subscribe($channel, $username, $pass, $host);

    if ($may_subscribe) {
      // save a record of this subscription
      $result = publish_subscription_save($channel_id, $username, $pass, $dest, $method, $filters);

      if (is_numeric($result)) {
        $response = array('status' => 'ok', 'sid' => $result);
      }
      else {
        return xmlrpc_error(905, $result);
      }
    }
    else {
      return xmlrpc_error(904, theme('publish_authentication_failure_message', $username));
    }
  }
  else {
    return xmlrpc_error(901, t('No channel available at this ID.'));
  }

  return $response;
}

function _publish_prepare_channel($data) {
  $channel = array();
  $channel['id'] = $data->channel_id;
  $channel['name'] = check_plain($data->name);
  $channel['description'] = check_plain($data->description);
  $channel['filters'] = array();
  $channel['nodes'] = array();
  
  $viewp = explode('-', $data->view);
  $view = views_get_view($viewp[0]);
  if ($viewp[1]) {
    $view->set_display($viewp[1]);
  }
  $filters = $view->get_items('filter');
  $efilters = array();
  $channel['vocabularies'] = array();
  
  if (!empty($data->vocabularies)) {
    $vocabs = unserialize($data->vocabularies);
    $vocabularies = taxonomy_get_vocabularies();
    foreach ($vocabs as $vid) {
      $channel['vocabularies'][$vid] = $vocabularies[$vid]->name;
    }
  }

  if (!empty($data->efilters)) {
    $efilters = unserialize($data->efilters);
  }
  foreach ($filters as $name => $filter) {
    if ($filter['table'] == 'node' && $filter['field'] == 'type') {
      foreach ($filter['value'] as $type) {
        if (!in_array($type, $channel['nodes'])) {
          $channel['nodes'][] = $type;
        }
      }
    }
    
    if (in_array($name, $efilters) && $filter['exposed']) {
      $channel['filters'][$name] = $filter['expose']['label'];
    }
  }
  
  $channel['authtype'] = $data->authtype;
  
  return $channel;
}

function publish_xmls_get_channels() {
  global $base_url;

  $result = db_query('SELECT * FROM {publish_channel} WHERE advertise = 1');
  $channels = array();
  while ($data = db_fetch_object($result)) {
    $channel = _publish_prepare_channel($data);
    $channels[$data->channel_id] = $channel;
  }

  return $channels;  
}

function publish_xmls_may_subscribe($channel_id, $username = '', $pass = '') {
  global $base_url;
  
  $ip = $_SERVER['REMOTE_ADDR'];
  $host = gethostbyaddr($ip);
  
  if ($channel_id) {
    $channel = publish_channel_load($channel_id);
  }
  
  if ($channel) {
    $may_subscribe = publish_may_subscribe($channel, $username, $pass, $host);
    if (!$may_subscribe) {
      return xmlrpc_error(904, theme('publish_authentication_failure_message', $username));
    }
  }
  else {
     return xmlrpc_error(901, t('No channel available at this ID.'));
  }

  $response = array($may_subscribe, _publish_prepare_channel($channel), $base_url);

  return $response;
}

/**
 * Decide if appropriate credentials have been given to subscribe
 * to a channel.
 *
 * @param $channel
 *   channel object (see publish_channel_load())
 *
 * @param $username
 *   A username as a string
 *
 * @param $pass
 *   A password as plain text
 *
 * @return
 *   Boolean
 */
function publish_may_subscribe($channel, $username, $pass, $domain) {
  $may_subscribe = FALSE;

  $domains = strstr($channel->domains, ',') ? explode($channel->domains, ',') : array($channel->domains);
  if ($channel->domains && !in_array($domain, $domains)) {
    return FALSE;
  }

  switch ($channel->authtype) {
    case PUBLISH_AUTHTYPE_NONE:
      $may_subscribe = TRUE;
      break;

    case PUBLISH_AUTHTYPE_PERM:
      $user = user_load(array('name' => $username, 'pass' => $pass));
      if (user_access('subscribe to channels', $user)) {
        $may_subscribe = TRUE;
      }
      break;

    case PUBLISH_AUTHTYPE_SIMPLE:
      if ($channel->username == $username && $channel->pass == $pass) {
        $may_subscribe = TRUE;
      }
      break;
  }
  return $may_subscribe;
}

function publish_cron() {
  publish_push();
}

function publish_theme($existing, $type, $theme, $path) {
  return array(
    'publish_authentication_failure_message' => array(
      'arguments' => array('username' => NULL),
    ),
  );
}

function theme_publish_authentication_failure_message($username) {
  return t('Message from %site_name: Authentication for user "%username" failed', array('%site_name' => variable_get('site_name', 'Drupal remote site'), '%username' => $username));
 }
