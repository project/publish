<?php
/**
 * @file
 * Admin functions for the Publish module.
 *
 */
 
function publish_subscribers($channel_id) {
  $channel = publish_channel_load($channel_id);
  
  drupal_set_title(t('Subscribers to channel !channel', array('!channel' => $channel->name)));
  
  $header = array(array('data' => t('Subscriber')), array('data' => t('Created')), array('data' => t('Status')), array('data' => t('Last update')), array('data' => 'Operations', 'colspan' => '2'));
  $rows = array();

  $result = db_query('SELECT * FROM {publish_subscribers} WHERE channel_id = %d', $channel_id);
  while ($data = db_fetch_object($result)) {
    // build table
    $rows[] = array(
      array('data' => $data->url),
      array('data' => format_date($data->created)),
      array('data' => $data->sub_status ? t('Enabled') : t('Disabled')), 
      array('data' => $data->last_sent == 0 ? t('Never') : format_date($data->last_sent)),
      array('data' => l('add nodes', 'admin/content/publish/subscribers/fill/'. $data->sid)),
      array('data' => l('delete', 'admin/content/publish/subscribers/delete/'. $data->sid))
    );
  }
  
  $output = count($rows) ? theme('table', $header, $rows) : '<p>' . t('No one is currently subscribed to this channel.') . '</p>';

  return $output;
}

function publish_admin_subscription_form_delete($form_state, $sid) {
  $subscription = publish_subscription_load($sid);
  if (!$subscription) {
    drupal_set_message(t('Unable to load the specified subscription.'));
    drupal_goto('admin/content/publish');
  }
  
  return confirm_form(
    array(
      'subscription' => array(
        '#type' => 'value',
        '#value' => $subscription,
      ),
    ),
    t('Are you sure you want to remove the subscription for %subscription?', array('%subscription' => $subscription->url)),
    'admin/content/publish',
    t('This action cannot be undone.'),
    t('Delete subscription'),
    t('Cancel')
  );
}

function publish_admin_subscription_form_delete_submit($form, &$form_state) {
  $subscription = $form_state['values']['subscription'];
  
  publish_subscription_delete($subscription->sid);
  
  drupal_set_message(t('Deleted subscription !subscription', array('!subscription' => $subscription->url)));
  watchdog('publish', 'Deleted channel !subscription', array('!subscription' => $subscription->url));
  
  $form_state['redirect'] = 'admin/content/publish';
}

function publish_overview() {
  $header = array(array('data' => t('ID')), array('data' => t('Name')), array('data' => t('Advertise')), array('data' => t('Subscribers')), array('data' => t('Operations'), 'colspan' => 2));

  $result = db_query('SELECT * FROM {publish_channel}');
  $row = array();

  while ($data = db_fetch_object($result)) {
    $count = db_result(db_query("SELECT COUNT(*) FROM {publish_subscribers} WHERE channel_id = %d", $data->channel_id));
    $row[] = array(
      array('data' => $data->channel_id, 'valign' => 'top'),
      array('data' => "$data->name", 'valign' => 'top'),
      array('data' => $data->advertise ? t('yes') : t('no'), 'valign' => 'top'),
      array('data' => $count ? l($count, 'admin/content/publish/subscribers/'. $data->channel_id) : t('none'), 'valign' => 'top'),
      array('data' => l(t('edit'), 'admin/content/publish/edit/'. $data->channel_id)),
      array('data' => l(t('delete'), 'admin/content/publish/delete/'. $data->channel_id)),
    );
  }

  $output = $row ? theme('table', $header, $row) : t('No channels created. Would you like to <a href="!add-channel">create one</a>?', array('!add-channel' => url('admin/settings/publish/add')));

  return $output;
}

function publish_admin_channel_form_add($form_state, $channel_id = NULL) {
  if ($channel_id) {
    $channel = publish_channel_load($channel_id);
    if (!$channel) {
      drupal_set_message(t('Unable to load the specified channel.'));
      drupal_goto('admin/content/publish');
    }
  }
  
  $views = array();
  $all_views = views_get_all_views();
  foreach ($all_views as $view) {
    if ($view->base_table == 'node') {
      foreach ($view->display as $display_name => $display) {
        $view->set_display($display_name);

        $has_argument = FALSE;
        $arguments = $view->get_items('argument');
        foreach ($arguments as $name => $argument) {
          if ($argument['table'] == 'node' && $argument['field'] == 'nid') {
            $has_argument = TRUE;
            break;
          }
        }
        if ($has_argument) {
          $views[$view->name .'-'. $display_name] = $view->name .' - '. $display->display_title;
        }
      }
    }
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#default_value' => isset($channel->name) ? $channel->name : '',
    '#size' => '60',
    '#maxlength' => '255',
    '#required' => TRUE,
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($channel->description) ? $channel->description : '',
    '#size' => '60',
    '#maxlength' => '255',
    '#description' => t('A brief description of this channel.'),
  );

  $form['advertise'] = array(
    '#type' => 'checkbox',
    '#title' => t('Advertise on channel list'),
    '#default_value' => isset($channel->advertise) ? $channel->advertise : '0',
    '#description' => t('Show name and description of this channel when another site asks which channels are available.'),
  );
  
  $sview = isset($channel->view) ? $channel->view : reset($views);

  $form['view'] = array(
    '#type' => 'select',
    '#title' => 'View',
    '#description' => t('This view will be used to filter nodes for this channel.'),
    '#options' => $views,
    '#default_value' => $sview,
    '#ahah' => array(
      'path' => 'admin/content/publish/js',
      'wrapper' => 'publish-view-info',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  
  $form += _publish_admin_views_element($sview, !empty($channel->efilters) ? unserialize($channel->efilters) : NULL);
  
  $form['authentication'] = array(
    '#type' => 'fieldset',
    '#title' => t('Who may subscribe to this channel?'),
  );

  $form['authentication']['authtype'] = array(
    '#type' => 'radios',
    '#title' => t('Choose an authorization model'),
    '#default_value' => isset($channel->authtype) ? $channel->authtype : PUBLISH_AUTHTYPE_NONE,
    '#options' =>  array(t('Open'), t('Users with !perm permission', array('!perm' => theme('placeholder', 'subscribe to channel'))), t('Username and password (enter below)')),
  );

  $form['authentication']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#maxlength' => '60',
    '#default_value' => isset($channel->username) ? $channel->username : '',
  );

  $form['authentication']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#maxlength' => '32',
    '#default_value' => isset($channel->pass) ? $channel->pass : '',
  );

  $form['domains'] = array(
    '#type' => 'fieldset',
    '#title' => t('Domains that may subscribe'),
  );

  $form['domains']['allowed'] = array(
    '#type' => 'textarea',
    '#title' => t('Domains'),
    '#rows' => '2',
    '#description' => t('Subscribers will be restricted to domains listed here, e.g. !example. If no domains are listed, all may subscribe.', array('!example' => theme('placeholder', 'example.org, example2.org'))),
  );

  if ($channel) {
    $form['channel_id'] = array(
      '#type' => 'value',
      '#value' => $channel->channel_id,
    );
  }

  $form['when'] = array(
    '#type' => 'fieldset',
    '#title' => t('How often do you want content published?'),
  );

  $form['when']['whenpub'] = array(
    '#type' => 'radios',
    '#title' => t('When should content be published?'),
    '#default_value' => isset($channel->whenpub) ? $channel->whenpub : PUBLISH_WHENPUB_CRON,
    '#options' => array(t('At regular intervals'), t('As soon as it is submitted/updated')),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => $channel ? t('Update channel info') : t('Save channel'),
  );
    
  return $form;
}

function _publish_admin_views_element($view, $selected = NULL) {
  $types = array();
  $ofilters = array();
  $vocabularies = array();
  $voptions = array();
  $viewp = explode('-', $view);

  $view = views_get_view($viewp[0]);
  if ($view) {
    if ($viewp[1]) {
      $view->set_display($viewp[1]);
    }
    $filters = $view->get_items('filter');
    foreach ($filters as $name => $filter) {
      if ($filter['table'] == 'node' && $filter['field'] == 'type') {
        foreach ($filter['value'] as $type) {
          $types[] = $type;
          $vocabularies = array_merge($vocabularies, taxonomy_get_vocabularies($type));
        }
      }
      if ($filter['exposed']) {
        $ofilters[$name] = $filter['expose']['label'];
      }
    }
  }
  
  if (!count($types)) {
    $vocabularies = taxonomy_get_vocabularies();
  }
  
  foreach ($vocabularies as $voc) {
    $voptions[$voc->vid] = $voc->name;
  }
  
  $form['view_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('View info'),
    '#prefix' => '<div id="publish-view-info">',
    '#suffix' => '</div>',
  );

  if (count($types)) {
    $form['view_info']['ntypes'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('This view publishes nodes of the following type(s): !types.', array('!types' => implode(', ', $types))) .'</p>',
    );
  }
  else {
    $form['view_info']['ntypes'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('This view does not limit the node types publised.') .'</p>',
    );
  }
  
  if (count($ofilters)) {
    $form['view_info']['efilters'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Exposed filters to use'),
      '#options' => $ofilters,
      '#default_value' => $selected ? $selected : array(),
      '#description' => t('Subscribers will be able to alter the selected filters in order to limit the nodes returned.')
    );
  }
  else {
    $form['view_info']['efilters'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('This view does not have exposed filters.') .'</p>',
    );
  }

  if (count($vocabularies)) {
    $form['view_info']['vocabularies'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Vocabularies to send'),
      '#options' => $voptions,
      '#default_value' => $selected ? $selected : array(),
      '#description' => t('Terms in the selected vocabularies will be sent to the subscribers.')
    );
  }
  else {
    $form['view_info']['vocabularies'] = array(
      '#type' => 'markup',
      '#value' => '<p>'. t('There are no vocabularies set up.') .'</p>',
    );
  }
  
  return $form;
}

function publish_admin_views_ahah() {
  $view = $_POST['view'];
  
  $form_element = _publish_admin_views_element($view);
  
  $form_state = array('submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  // Add the new element to the stored form. Without adding the element to the
  // form, Drupal is not aware of this new elements existence and will not
  // process it. We retreive the cached form, add the element, and resave.
  if (!$form = form_get_cache($form_build_id, $form_state)) {
    exit();
  }
  $form['view_info'] = $form_element['view_info'];
  form_set_cache($form_build_id, $form, $form_state);
  $form += array(
    '#post' => $_POST,
    '#programmed' => FALSE,
  );

  // Rebuild the form.
  $form = form_builder('publish_admin_channel_form_add', $form, $form_state);

  // Render the new output.
  $ef_form = $form['view_info'];
  unset($ef_form['#prefix'], $ef_form['#suffix']); // Prevent duplicate wrappers.
  $output = theme('status_messages') . drupal_render($ef_form);

  drupal_json(array('status' => TRUE, 'data' => $output));
}

function publish_admin_channel_form_add_validate($form, &$form_state) {
  if ($form_state['values']['allowed'] != '') {
    $domain_pattern = "/([[:alpha:]][-[:alnum:]]*[[:alnum:]])(\.[[:alpha:]][-[:alnum:]]*[[:alpha:]])+/i";
    $domains = explode(',', $form_state['values']['allowed']);
    $err_domains = array();

    foreach ($domains as $domain) {
      $domain = trim($domain);
      if (!preg_match($domain_pattern, $domain)) {
        $err_domains[] = $domain;
      }
    }

    if ($err_domains != '') {
      form_set_error('allowed', t('The following domains are invalid: !domains.', array('!domains' => implode(', ', $err_domains))));
    }
  }
  
  if ($form_state['values']['authtype'] == PUBLISH_AUTHTYPE_SIMPLE) {
    if (trim($form_state['values']['username']) == '') {
      form_set_error('username', t('When using username based authentication you need to specify a username.'));
    }
    if (trim($form_state['values']['pass']) == '') {
      form_set_error('pass', t('When using username based authentication you need to specify a password.'));
    }
  }
}

function publish_admin_channel_form_add_submit($form, &$form_state) {
  if (isset($form_state['values']['channel_id'])) {
    $message = 'Updated channel !channel';
  }
  else {
    $message = 'Added channel !channel';
  }

  if (is_array($form_state['values']['efilters'])) {
    $form_state['values']['efilters'] = serialize($form_state['values']['efilters']);
  }
  else {
    $form_state['values']['efilters'] = serialize(array());
  }

  if (is_array($form_state['values']['vocabularies'])) {
    $form_state['values']['vocabularies'] = serialize($form_state['values']['vocabularies']);
  }
  else {
    $form_state['values']['vocabularies'] = serialize(array());
  }
  
  publish_channel_save($form_state['values']);

  drupal_set_message(t($message, array('!channel' => $form_state['values']['name'])));
  watchdog('publish', $message, array('!channel' => $form_state['values']['name']));

  $form_state['redirect'] = 'admin/content/publish';
}

function publish_admin_channel_form_delete($form_state, $channel_id) {
  $channel = publish_channel_load($channel_id);
  if (!$channel) {
    drupal_set_message(t('Unable to load the specified channel.'));
    drupal_goto('admin/content/publish');
  }
  
  return confirm_form(
    array(
      'channel' => array(
        '#type' => 'value',
        '#value' => $channel,
      ),
    ),
    t('Are you sure you want to remove the channel %channel?', array('%channel' => $channel->name)),
    'admin/content/publish',
    t('This action cannot be undone.'),
    t('Delete channel'),
    t('Cancel')
  );
}

function publish_admin_channel_form_delete_submit($form, &$form_state) {
  $channel = $form_state['values']['channel'];
  
  publish_channel_delete($channel->channel_id);
  
  drupal_set_message(t('Deleted channel !channel', array('!channel' => $channel->name)));
  watchdog('publish', 'Deleted channel !channel', array('!channel' => $channel->name));
  
  $form_state['redirect'] = 'admin/content/publish';
}

function publish_admin_subscription_fill($sid) {
  $subscription = publish_subscription_load($sid);
  $channel = publish_channel_load($subscription->channel_id);
  
  $viewp = explode('-', $channel->view);
  $view = views_get_view($viewp[0]);
  $view->set_display($viewp[1]);
  $view->pager['items_per_page'] = 0;
  $view->execute();
  
  $cnt = 0;
  
  foreach ($view->result as $node) {
    publish_queue_delete($node->nid, $sid);
    publish_queue_save($node->nid, $subscription->sid, $subscription->channel_id, time());
    $cnt++;
  }
  
  drupal_set_message(t('Added !count nodes to the queue.', array('!count' => $cnt)));
  drupal_goto('admin/content/publish/subscribers/'. $subscription->channel_id);
}