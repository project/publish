********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Publish Module
Authors: 
- Alexandru Badiu <i at voidberg dot org>: Drupal 6 version
- John VanDyk <jvandyk at iastate dot edu>: Drupal 4.7 version

********************************************************************
DESCRIPTION:

The publish module allows you to set up one or more channels through
which nodes can flow. A Drupal installation using the subscribe
module can then subscribe to one of the channels and receive nodes.

New nodes can be sent along the channel as soon as they are
created, they can be sent when cron runs, or the subscribing site
can request an update. These options can be changed on a per-
subscription basis.

The publish module uses XML-RPC to transport the nodes. Thus, any
XML-RPC-capable client can subscribe to a channel. Of course, the
non-Drupal client may need to be told what to do with a node once it
arrives. I test the publish module using a Userland Frontier(TM)
implementation. To see the XML-RPC methods that are implemented,
see publish_xmlrpc() or ask your Drupal site by calling it via
XML-RPC using the system.listMethods method:

$result = xmlrpc ('http://example.com/xmlrpc.php',
  'system.listmethods');
print_r($result);

Since you may not want to allow everyone in the world to subscribe
to your channel, you have your choice of authentication methods
that a subscribing site must use. You can have no authentication,
password-based authentication, or role-based authentication.

When setting up a channel, you must specify which of the node types
available on your site (e.g. story, page) should be published, as
well as which vocabularies should be published.

********************************************************************
INSTALLATION:

1. Place the entire publish folder in the modules folder of
   your Drupal installation.
   
2. Go to administer > modules and enable the Publish module.

********************************************************************
GETTING STARTED:

TBD

********************************************************************
